function clockThread(){
	var timeout = 1000;
	//i do not use this var
	var date = new Date(); 
	//take hour,minutes and seconds
	var hour = date.getHours();
	var minutes = date.getMinutes();
	var seconds = date.getSeconds();
	
	//add zero if length are 1 char
	if(String(hour).length == 1){
		hour = "0"+String(hour);
	}
	if(String(minutes).length == 1){
		minutes = "0"+String(minutes);
	}
	if(String(seconds).length == 1){
		seconds = "0"+String(seconds);
	}
	//output on label
	var label = document.getElementById("real_time_clock");
	label.innerHTML= String(hour) + ":" + String(minutes) + ":" + String(seconds);
	//start thread with timeout of 1000ms == 1sec
	setTimeout(clockThread, timeout);
}

