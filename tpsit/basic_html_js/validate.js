function validate(){
	var forms = document.forms['data'];
	for(x=0;x<forms.length-7;x++){
			if(forms[x].value == ""){
				alert(forms[x].name + " non e' valido, cambia il valore");
				return false;
			}
	}
	for(x=0;x<2;x++){
		var alpha = isAlpha(forms[x].value);
		if(!(alpha)){
			alert(forms[x].name + " non contiene solo caratteri alfabetici");
			return false;
		}
	}
	//javascript to generate cf function
	if(isNaN(forms[4].value)){
		alert(forms[4].name + " non contiene solo numeri");
		return false;
	}
	if(forms[4].value.length != 10){
		alert(forms[4].name + " non ha il giusto formato");
		return false;
	}
	if(!(forms[7].checked)){
		alert(forms[8].name + " non hai accettato le condizioni");
		return false;
	}

	return true;
}
