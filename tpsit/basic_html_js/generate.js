//functions that check if a string var is ONLY alphabetichal, vocal or if is not a string
function isAlpha(ch){
	ch = ch.replace(" ","");
	if (typeof ch === "string"){
		for(y=0;y<ch.length;y++){
			//alert(ch[x] < "A" || (ch[x] > "Z" && ch[x] < "a") || ch[x] > "z");
			if(ch[y] < "A" || (ch[y] > "Z" && ch[y] < "a") || ch[y] > "z"){
				return false;
			}
		}
		return true;
	}
		return false;

}

function isVocal(char){
	if(char == "a" || char == "e" || char == "i" || char == "o" || char == "u"){
		return true;
	}
	return false;
}

function isNaNstr(char){
	if(char < "1" || char > "9"){
		return true;
	}
	else{
		return false;
	}
}

//return month char, it is used a JSON dictionary data structure
function month(m){
	var dict = {
		"01": "A",
		"02": "B",
		"03": "C",
		"04": "D",
		"05": "E",
		"06": "H",
		"07": "L",
		"08": "M",
		"09": "P",
		"10": "R",
		"11": "S",
		"12": "T"
	};
	return dict[m];
}

function nameGenerator(surname){
	//surname in a name function is a bid for power
	var surname = surname.toLowerCase();
	var result = "";
	var cons = "";
	var voc = "";
	//counters
	var y = 0
	var a = 0
	var b = 0
	//while vocals are < 4 or consonants are < 4 add consonant to cons and vocals to voc
	while(y<surname.length && (a < 4 || b < 4) ){
		if(!isVocal(surname[y]) && isNaNstr(surname[y])){
			{
				cons = cons + surname[y];
				a++;
			}
		}
		else if(isVocal(surname[y])){
			voc = voc + surname[y];
			b++;
		}
		y++;
	}
	//if there are 0-3 consonant create result with all consonant and all vocal and if result is < 3 add Xs
	if(cons.length < 4){
		for(y=0;y<cons.length;y++){
			result = result + cons[y];
		}
		for(y=0;y<voc.length && result.length < 3;y++){
			result = result + voc[y];
		}
		while(result.length < 3)
			result = result + "x";
	}
	//best case if we have >3 consonants i will add first third and fourth cons to cf
	else if (cons.length > 3){
		result = cons[0]+cons[2]+cons[3];
	}
	return result;
}

//watch name generator, only difference signed
function surnameGenerator(surname){
	var surname = surname.toLowerCase();
	var result = "";
	var cons = "";
	var voc = "";
	var y = 0
	var a = 0
	var b = 0
	while(y<surname.length && (a < 3 || b < 3) ){
		if(!isVocal(surname[y]) && isNaNstr(surname[y])){
			cons = cons + surname[y];
			a++;
		}
		else if(isVocal(surname[y])){
			voc = voc + surname[y];
			b++;
		}
		y++;
	}
	//<3 cause are used the firts 3 cons
	if(cons.length < 3){
		for(y=0;y<cons.length;y++){
			result = result + cons[y];
		}
		for(y=0;y<voc.length && result.length < 3;y++){
			result = result + voc[y];
		}
		while(result.length < 3)
			result = result + "x";
	}
	else if (cons.length > 2){
		result = cons[0]+cons[1]+cons[2];
	}
	return result;
}

function checkData(data, cf){
	//today date 
	var today = new Date();
	//if year > today not born
	if((data.value).split("-")[0] > today.getFullYear()){
		alert("ancora non nato");
		return "BackToTheFuture";
	}
	//if year is equal not born and month > todaymonth+1 [month starts by 0] not born
	else if( (data.value).split("-")[0] == today.getFullYear() && Number((data.value).split("-")[1]) > Number(today.getMonth()) + Number(1)){
		alert("ancora non nato");
		return "BackToTheFuture";
	}
	//if year and month are equal but date > of today date not born
	else if( (data.value).split("-")[0] == today.getFullYear() && Number((data.value).split("-")[1]) == Number(today.getMonth())+Number(1) && (Number((data.value).split("-")[2]) > Number(today.getDate()))){
		alert("ancora non nato");
		return "BackToTheFuture";
	}
	//if here you're born congratulations
	return cf;

}
//this function were called only after check, then i do not check for input errors here
function generate(){
	//initial variable cf initialized with values of name and surname, it will take all data
	var cf = surnameGenerator(document.forms[0][0].value.replace(" ","")) + nameGenerator(document.forms[0][1].value.replace(" ",""));
	cf = cf + ((document.forms[0][2].value).split("-"))[0].slice(-2);
	cf = cf + month((document.forms[0][2].value).split("-")[1]);
	var y = 0; if(document.forms[0][6].checked) {y=40};
	cf = checkData(document.forms[0][2], cf);	
	//if data checked correctly
	if(cf != "BackToTheFuture"){
		add = String(( Number((document.forms[0][2].value).split("-")[2]) + Number(y)));
		//add zero to date birthday
		if(add.length == 1){
			add = "0" + String(add);
		}
		cf = cf + add;
	}
	//send data to label and return true
	document.getElementById("cf").innerHTML = cf.toUpperCase();
	return true;
}
