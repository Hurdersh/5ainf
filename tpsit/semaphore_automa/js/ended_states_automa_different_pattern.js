function automa(){
	//i will represent automa as a graph
	//[green orange red] 
	states = [
		[false,false,false],
		[false,true,false],
		[false,false,true],
		[true,true,false],
		[true,false,false],
		[true,false,true],
		[false,true,true],
		[true,true,true]
	]
	//i do a list of a list because it can be more of 1 nodes representing states :),
	//don't check this at error
	adiacenty = [
		[states[4]], //state 0 adiacenty
		[states[7]], //state 1 adiacenty
		[states[5]], //etc
		[states[2]],
		[states[1]],
		[states[6]],
		[states[3]],
		[states[0]]

	];
	//all is global cause i can only use setInterval function and it cann pass argument
	//to functions

	//infinity loop, potentially you can create a thread with setTimeout(function)
	setInterval(looping_automa,2000);
}

temp = [[false,false,true]];
//queue state and adiacenty mutex
qsm = false; 
adm = false;
//why javascript is shit
//while(queue_state > 0) with custom sleep 10 seconds function obviously working(i'm tried with alerts) is releaved as 
//warning function, but setTimeout of javascript with 2 seconds no, and i must use the temp global var
//to memorize state of automa , if i can remain in the loop the world can be happy
function looping_automa(){
	var red = document.getElementById("red");
	var green = document.getElementById("green");
	var orange = document.getElementById("orange");
	//mutex access
	while(qsm == true){} //i'll check it
	qsm = true; //i'll take it
	queue_state = temp;
	if (queue_state.length > 0){
			
		if(queue_state[0][0] == true)
			green.src="img/semaphore-light-green.png";
		else
			green.src="img/semaphore-light.png";
		
		if(queue_state[0][1] == true)
			orange.src="img/semaphore-light-orange.png";
		else
			orange.src="img/semaphore-light.png";
		if(queue_state[0][2] == true)
			red.src="img/semaphore-light-red.png";
		else
			red.src="img/semaphore-light.png";
		//out from the vector(implemented doubly linked list, congrats js developers)
		
		//and javascript indexof don't work by array of array
		var z = 0;
		var indexofmine = -1;
		while(z < states.length && indexofmine == -1){
			if (JSON.stringify(states[z]) === JSON.stringify(queue_state[0]))
				indexofmine = z;
			z=z+1;
		}
		while(adm == true){}
		adm = true;
		if(indexofmine != -1){
			for(y = 0; y < adiacenty[indexofmine].length; y++){
				queue_state.push(adiacenty[indexofmine][y]);
			}
		}
		adm = false;
		queue_state.shift();
		//end of mutual exclusive access
		qsm = false;
		temp = queue_state;	
	}
}

function semaphore_on_off_toggle(){
	//change state and queue
	while(adm == true || qsm == true){}
	adm = true;
	qsm=true;
	//not use mutex for states cause states are ended
	if(JSON.stringify(adiacenty[1]) === JSON.stringify([states[2]])){
		adiacenty[1] = [states[0]];
		temp = [states[1]];
	}
	else{
		adiacenty[1] = [states[2]];
		temp = [states[2]];
	}
	adm=false;
	qsm=false;
}

