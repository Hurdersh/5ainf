function drawme(){
	//resize
	var canvas = document.getElementById("canvas");
	var context = canvas.getContext("2d");
	canvas.width = window.innerWidth-(window.innerWidth/100*2);
	canvas.height = window.innerHeight-(window.innerHeight/100*3);
	var proportional = canvas.width/100;
	//images
	var image = new Image();
	var frame = 1;
	var keyPressed = {};
	function charge(){
		if(frame >= 0 && frame <= 120){
			image.src = "img/ball-animation/"+String(frame)+".png";
			window.requestAnimationFrame(drawframe);
		}
		if( frame < 120 && keyPressed[39] == true){
			frame+=1;
		}
		if(frame > 1 && keyPressed[37] == true){
			frame-=1;
		}

	}
	function drawframe(){
		context.clearRect(0,0,canvas.width,canvas.height);
		context.drawImage(image,0,0,proportional*100,proportional*60)
	}

	function onKeyDown(e){
		keyPressed[e.keyCode] = true;
	}

	function onKeyUp(e){
		keyPressed[e.keyCode] = false;
	}

	window.addEventListener('keydown', onKeyDown);
	window.addEventListener('keyup', onKeyUp);
	setInterval(charge, 1000/60);


}
