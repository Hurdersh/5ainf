function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}



function open_close_nav(){
  var sidebar = document.getElementById("sb");
  //all for mobile
  if(window.innerWidth > 600){
    /*var divs = document.querySelectorAll("div:not(.sidebar)");*/ //this is a good function, best code for another use
    //replace function queryall 'cause i don't need depth first search in-order
    var temp = document.body.firstChild;
    var divs = [];
    while (temp) {
      if(temp.tagName == "DIV" && !(temp.className.match(/\bsidebar\b/)) ){
          divs.push(temp);
      }
      temp = temp.nextSibling;
    }

    var footer = document.getElementsByClassName("footer");

    if(sidebar.style.width == ""){

      sidebar.style.width = "20vw";
      for(var x = 0; x < divs.length; x++){
        divs[x].style.marginLeft = "20vw";
      }
      //redimension footer
      footer[0].style.width = "79.8%";
    }

    else{
      sidebar.style.width = "";
      for(var x = 0; x < divs.length; x++){
        divs[x].style.marginLeft = "";
      }
      //redimension footer
      footer[0].style.width = "100%";
    }
  }
  else{
    if(sidebar.style.width == ""){
      sidebar.style.width = "100%";
    }

    else{
      sidebar.style.width = "";
    }
    
  }
}
