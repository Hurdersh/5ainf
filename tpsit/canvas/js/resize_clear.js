function resize_and_clear(){
	var canvas = document.getElementById("canvas");
	var context = canvas.getContext("2d");

	//dinamic redimensioning of canvas
	var canvas_width = window.innerWidth - (window.innerWidth/100*1.3);
	var canvas_height = window.innerHeight - (window.innerHeight/100*3.4);
	canvas.width = canvas_width;
	canvas.height = canvas_height;
}
