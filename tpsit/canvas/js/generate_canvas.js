function generate(){
	var canvas = document.getElementById("canvas");
	var context = canvas.getContext("2d");

	var font_size = canvas.height/100*5;

	//data about resizing
	var columns = canvas.width/font_size;
	var drops = [];
	for(var x = 0; x < columns; x++)
		drops[x] = 1;

	var font_family = "arial";
	var font_family_matrix = "monospace";
	var title = "MATRIX";
	var spacing = canvas.width/100*15;
	function draw(){
		var font_matrix = canvas.height/100*30;
		//draw background
		context.fillStyle = "rgba(0, 0, 0, 0.05)";
		context.fillRect(0,0,canvas.width,canvas.height);
		//draw my matrix Title
		context.font = String(font_matrix) + "px " + font_family;
		context.fillStyle = "#0F0";
		var startx = (canvas.width - ((title.length-1)*spacing))/2;
		startx = startx - startx/100*40;
		for(var x = 0;x<title.length-1;x++){
			context.fillText(title[x],startx,canvas.height/100*55);
			startx = startx+spacing;
		}
		context.fillStyle = "red";
		context.fillText(title[title.length-1],startx,canvas.height/100*55);

		context.font = String(font_size) + "px " + font_family;
		context.fillStyle = "#0F0";	
		for(var i=0;i< drops.length;i++){
			var text = String(Math.floor(Math.random()*9));
			context.fillText(text, i*font_size, drops[i]*font_size);
			if(drops[i]*font_size > canvas.height && Math.random() > 0.975)
				drops[i] = 0;
			drops[i]++;
		}

	}
	setInterval(draw,34);
}

