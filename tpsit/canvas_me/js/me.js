function drawme(){
	//resize
	var canvas = document.getElementById("canvas");
	var context = canvas.getContext("2d");
	canvas.width = window.innerWidth-(window.innerWidth/100*2);
	canvas.height = window.innerHeight-(window.innerHeight/100*3);
	var proportional = canvas.width/100;

	//testa
	context.beginPath();
	context.fillStyle = '#ffbb99';
	context.strokeStyle = 'black';
	context.lineWidth = proportional*0.2;
	context.arc(proportional*50,proportional*7,proportional*4,0,2*Math.PI);
	context.fill();
	context.stroke();
	context.closePath();

	//occhi
	context.beginPath();
	context.lineWidth = proportional*0.09;
	context.fillStyle = 'white';
	context.ellipse(proportional*48.3,proportional*6,proportional,proportional-proportional/3,0,0,2*Math.PI);
	context.fill();
	context.stroke();
	context.closePath();

	context.beginPath();
	context.fillStyle = 'white';
	context.ellipse(proportional*51.6,proportional*6,proportional,proportional-proportional/3,0,0,2*Math.PI);
	context.fill();
	context.stroke();
	context.closePath();


	context.beginPath();
	context.fillStyle = 'brown';
	context.arc(proportional*48.3, proportional*6, proportional - proportional/1.5,0,2*Math.PI);
	context.fill();
	context.stroke();
	context.closePath();


	context.beginPath();
	context.fillStyle = 'brown';
	context.arc(proportional*51.55, proportional*6, proportional - proportional/1.5,0,2*Math.PI);
	context.fill();
	context.stroke();
	context.closePath();

	//naso
	context.beginPath();
	context.ellipse(proportional*49.2, proportional*6.3, proportional*2, proportional*1, 1.5*Math.PI, 0.6*Math.PI, Math.PI);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(proportional*49.4, proportional*8.3);
	context.lineTo(proportional*50, proportional*8.3);
	context.stroke();
	context.closePath();

	//capelli
	context.beginPath();
	context.fillStyle = 'brown';
	context.ellipse(proportional*50,proportional*5.1,proportional*3.4,proportional*2,0,1*Math.PI,0);
	context.fill();
	context.closePath();

	//bocca (le labbra sono troppo lunghe da realizzare
	context.beginPath();
	context.lineWidth = proportional*0.1;
	context.moveTo(proportional*48.8,proportional*9.3);
	context.lineTo(proportional*51, proportional*9.3);
	context.stroke();
	context.closePath();
	

	//busto(cappotto)
	context.beginPath();
	context.lineWidth = proportional*0.2;
	context.fillStyle= 'black';
	//not maurizio costanzo
	context.moveTo(proportional*49,proportional*11);
	context.lineTo(proportional*49,proportional*13);
	context.lineTo(proportional*43,proportional*13);
	context.lineTo(proportional*37,proportional*18);
	context.lineTo(proportional*38.3,proportional*19.3);
	context.lineTo(proportional*43.5,proportional*15);
	context.lineTo(proportional*46.5,proportional*14.9);
	context.lineTo(proportional*46.5,proportional*27);
	context.lineTo(proportional*54,proportional*27);
	context.lineTo(proportional*54,proportional*15);
	context.lineTo(proportional*57,proportional*15);
	context.lineTo(proportional*62,proportional*19.3);
	context.lineTo(proportional*63.5,proportional*18);
	context.lineTo(proportional*57.2,proportional*13);
	context.lineTo(proportional*51.3,proportional*13);
	context.lineTo(proportional*51.3,proportional*11);
	context.lineTo(proportional*49,proportional*11);	
	context.stroke();
	context.fill();
	context.closePath();
	
	//pantaloni
	context.beginPath();
	context.fillStyle = "purple";
	context.moveTo(proportional*46.5,proportional*27);
	context.lineTo(proportional*44, proportional*40);
	context.lineTo(proportional*47, proportional*40);
	context.lineTo(proportional*50.2, proportional*28);
	context.lineTo(proportional*53, proportional*40);
	context.lineTo(proportional*56, proportional*40);
	context.lineTo(proportional*54, proportional*27);
	context.lineTo(proportional*46.5, proportional*27);
	context.fill();
	context.stroke();
	context.closePath();

	function mano(x, y, cxt, rot){
		cxt.beginPath();
		cxt.fillStyle = '#ffbb99';
		cxt.ellipse(x,y,proportional*1.2,proportional*1,rot,0,2*Math.PI);
		cxt.fill();
		cxt.stroke();
		cxt.closePath();
	}
	function scarpa(x, y, cxt, rot){
		cxt.beginPath();
		cxt.fillStyle = 'blue';
		cxt.ellipse(x,y,proportional*2.2,proportional*1,rot,0,2*Math.PI);
		cxt.fill();
		cxt.stroke();
		cxt.closePath();
	}

	mano(proportional*63.5,proportional*19.5,context,56);
	mano(proportional*36.9,proportional*19.5,context,(0.5*Math.PI)/2.3);
	scarpa(proportional*44.7,proportional*41.1,context,0);
	scarpa(proportional*55,proportional*41.1,context,0);

}
