class map{
  constructor(filename){
    this.img = new Image();
    this.img.src = filename;
    this.x = 0;
    this.toggle = false;
    this.interval = 0;
  }
  draw(canvas_id){
    var self = this;
    var canvas = document.getElementById(canvas_id);
    var context = canvas.getContext('2d');
    if (this.img.complete){
      context.drawImage(self.img,self.x,0,window.innerWidth,window.innerHeight,0,0,window.innerWidth,window.innerHeight);
    }
  }

  getx(){
    return this.x;
  }

  autoIncrementxToggle(y,fps){
    var self = this;
    if (this.toggle == false){
      this.interval = setInterval(function(){self.x=self.x+y;},1000/fps);
      this.toggle = true;
    }
    else {
      clearInterval(this.interval);
      this.toggle = false;
    }
  }


}
