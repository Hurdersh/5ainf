function listeners(){
  //event listeners
  document.addEventListener('keyup', keyUp);
  document.addEventListener('keydown', keyDown);

  //key tracks
  var right_arrow = false;
  var left_arrow = false;
  var map_moving = false;
  var down_arrow = false;

  //function and implementations
  function keyUp(e) {
    if (e.keyCode == 40 && protagonista.getImageIndex() == 4 && down_arrow == true){
      protagonista.changeImage(0);
      protagonista.setHeight(window.innerHeight/100*20);
      protagonista.sety(protagonista.gety()-window.innerHeight/100*5);
      down_arrow = false;
    }
    if (e.keyCode == 40 && protagonista.getImageIndex() == 5 && down_arrow == true){
      protagonista.changeImage(1);
      protagonista.setHeight(window.innerHeight/100*20);
      protagonista.sety(protagonista.gety()-window.innerHeight/100*5);
      down_arrow = false;
    }
    if (e.keyCode == 39 && map_moving == true){
      mappa.autoIncrementxToggle(2,60);
      map_moving = false;
    }
    if (e.keyCode == 39 && right_arrow == true){
      protagonista.autoIncrementxToggle(2,60);
      right_arrow = false;
    }
    if (e.keyCode == 37 && left_arrow == true){
      protagonista.autoDecrementxToggle(2,60);
      left_arrow = false;
    }
  }

  function keyDown(e) {
    if (e.keyCode == 40 && protagonista.getImageIndex() == 0 && down_arrow == false && right_arrow == false){
      protagonista.changeImage(4);
      protagonista.setHeight(window.innerHeight/100*15);
      protagonista.sety(protagonista.gety()+window.innerHeight/100*5);
      down_arrow = true;
    }
    if (e.keyCode == 40 && protagonista.getImageIndex() == 1 && down_arrow == false && right_arrow == false){
      protagonista.changeImage(5);
      protagonista.setHeight(window.innerHeight/100*15);
      protagonista.sety(protagonista.gety()+window.innerHeight/100*5);
      down_arrow = true;
    }
    if (e.keyCode == 17 && protagonista.getImageIndex() == 0){
      protagonista.setWidth(window.innerWidth/100*9);
      protagonista.changeImage(2);
      setTimeout(function(){protagonista.changeImage(0);protagonista.setWidth(window.innerWidth/100*7);},200);
    }
    if (e.keyCode == 17 && protagonista.getImageIndex() == 1){
      protagonista.setWidth(window.innerWidth/100*10)
      protagonista.setx(protagonista.getx()-window.innerWidth/100*3);
      protagonista.changeImage(3);
      setTimeout(function(){protagonista.changeImage(1);protagonista.setWidth(window.innerWidth/100*7);
      protagonista.setx(protagonista.getx()+window.innerWidth/100*3);},200);
    }
    if (e.keyCode == 39 && protagonista.getx() >= canvas.width/2 && map_moving == false){
      mappa.autoIncrementxToggle(2,60);
      map_moving = true;
    }
    if (e.keyCode == 39 && right_arrow == false && down_arrow == false){
      protagonista.changeImage(0);
      protagonista.autoIncrementxToggle(2,60);
      right_arrow = true;
    }
    if (e.keyCode == 37 && left_arrow == false && down_arrow == false){
      protagonista.changeImage(1);
      protagonista.autoDecrementxToggle(2,60);
      left_arrow = true;
    }
  }
}
