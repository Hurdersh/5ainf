class drawing{
  constructor(canvas_id,elements,fps){
    this.canvas_id = canvas_id;
    this.elements = elements;
    this.fps = fps;
  }

  toggleDraw(){
    self = this;
    var canvas = document.getElementById(this.canvas_id);
    var context = canvas.getContext('2d');
    //convert fps to millisecond 1000/fps
    setInterval(function(){
      //context.clearRect(0,0,canvas.width,canvas.height);
      for(var x = 0;x<self.elements.length;x++){
        self.elements[x].draw(self.canvas_id);
      }
    },(1000/this.fps));
  }
}
