class figure{
  constructor(filenames){
    this.images = new Array(filenames.length);
    for(var i = 0;i<filenames.length;i++){
      this.images[i] = new Image();
      this.images[i].src = filenames[i];
    }
    this.img = 0;
    this.x = 0;
    this.y = 0;
    this.hitboxx = 0;
    this.hitboxy = 0;
    this.toggle = [false,false,false,false,false];
    this.intervals = [];
    this.width = window.innerWidth/100*7;
    this.height = window.innerHeight/100*20;
  }
  draw(canvas_id){
    var self = this;
    var canvas = document.getElementById(canvas_id);
    var context = canvas.getContext('2d');
    if (this.images[this.img].complete){
      context.drawImage(self.images[self.img],this.x,this.y,this.width,this.height);
    }
  }

  getx(){
    return this.x;
  }

  gety(){
    return this.y;
  }

  setx(x){
    this.x = x;
  }

  sety(y){
    this.y = y;
  }

  autoIncrementxToggle(x,fps){
    var canvas = document.getElementById("canvas");
    if (this.toggle[0] == false){
      console.log("creating");
      var self = this;
      this.intervals[0] = setInterval(function(){
        if(self.x < canvas.width/2){
          self.x=self.x+x;
          self.hitboxx = self.x;
        }},1000/fps);
      this.toggle[0] = true;
    }
    else{
      console.log("clearing");
      clearInterval(this.intervals[0]);
      this.toggle[0] = false;
    }
  }

  autoDecrementxToggle(x,fps){
    if (this.toggle[1] == false){
      console.log("creating");
      var self = this;
      this.intervals[1] = setInterval(function(){
        if(self.x-x > 0){
        self.x=self.x-x;
        self.hitboxx = self.x;}},1000/fps);
      this.toggle[1] = true;
    }
    else{
      console.log("clearing");
      clearInterval(this.intervals[1]);
      this.toggle[1] = false;
    }
  }

  changeImage(index){
    this.img = index;
  }

  getImageIndex(){
    return this.img;
  }

  setWidth(width){
    this.width = width;
  }
  setHeight(height){
    this.height = height;
  }
}
