//global variable representing state, no mutex needed
state=0;
//another global variable states representing states
//[goodinput,title,background image, [button_image,attributes]]
states = [
	[1,"Stage1: where is ornitorinco? ", "img/insiemi-background.png",
		[ ["src","img/insiemi-universe.png"],["id","insuni"],["onclick","generate(0)"] ],
		[ ["src","img/insiemi-intersezione.png"], ["id","insunion"], ["onclick","generate(1)"] ],
		[ ["src","img/insiemi-mammiferi.png"], ["id","insmamm"], ["onclick", "generate(2)"]  ],
		[ ["src","img/insiemi-ovovipari.png"], ["id","insovo"], ["onclick", "generate(3)"] ] 
	],

	[2,"Stage2: select complexity for merge sort O(log(n))", "img/white.jpeg", 
		[ ["src","img/nplot.png"], ["id","nplot"], ["onclick","generate(0)"] ],
		[ ["src","img/doublenplot.png"],  ["id","doublenplot"], ["onclick", "generate(1)"]],
		[ ["src","img/logplot.png"],  ["id","logplot"], ["onclick", "generate(2)"]], 
		[ ["src","img/oneplot.png"],  ["id","oneplot"], ["onclick", "generate(3)"]]
	],
	//hey, i copied myself code, i'm stackoverflow
	[1,"Stage3: select what kind of ended state automa in use for this exercise", "img/white.jpeg", 
		[ ["src","img/esa0.png"], ["id","esa0"], ["onclick","generate(0)"] ],
		[ ["src","img/esa1.png"],  ["id","esa1"], ["onclick", "generate(1)"]],
		[ ["src","img/esa2.png"],  ["id","esa2"], ["onclick", "generate(2)"]], 
		[ ["src","img/esa3.png"],  ["id","esa3"], ["onclick", "generate(3)"]]
	],
	[3,"Stage4: select recursive algorith to find factorial of an n number", "img/white.jpeg", 
		[ ["src","img/gcd.png"], ["id","gcd"], ["onclick","generate(0)"] ],
		[ ["src","img/factorialnr.png"],  ["id","factorialnr"], ["onclick", "generate(1)"]],
		[ ["src","img/hanoi.png"],  ["id","hanoi"], ["onclick", "generate(2)"]], 
		[ ["src","img/factorial.png"],  ["id","factorial"], ["onclick", "generate(3)"]]
	],
	[0, "Final Stage: YOU WIN, Congrats[click to return to begin]", "img/win.gif", [ ["src","img/white.jpeg"], ["id","white"], ["onclick","generate(100)"]] ]
];
function generate(input){
	//there is a good input
	if(input == states[state][0]){
		//i do this 'cause you do not want association but only progressive states
		state = state+1;
	}
	else{
		state = 0;
	}
	//delete all elements of scene(removeChild or innerhtml) i will use removechild to demonstrate i can
	//creation of title
	z = 0
	var titletag = document.createElement("H3");
	var scene = document.getElementsByClassName("scene")[0];
	titletag.setAttribute("class","gametitle");
	while(scene.firstChild){
		scene.removeChild(scene.firstChild);
	}
	scene.appendChild(titletag);
	setInterval(title,100);
	//set background
	/*
	 * canvas was deprecated from this project, long life to images
	canvas = document.getElementsByClassName("background-scene")[0];
	canvas.width = window.innerWidth-40;
	canvas.height = window.innerHeight-80;
	var canvas_context = canvas.getContext("2d");
	var background = new Image();
	background.src=states[state][2];
	background.onload = () => {  canvas_context.drawImage(background, 0, 0, canvas.width, canvas.height)}
	document.getElementsByTagName("BODY")[0].onresize = function(){
		canvas.width = window.innerWidth-40;
		canvas.height = window.innerHeight-80;
		canvas_context.drawImage(background, 0, 0, canvas.width, canvas.height);
	}
	*/
	
	var background = document.createElement("IMG");
	background.setAttribute("src", states[state][2]);
	background.setAttribute("id", "background-scene");
	scene.appendChild(background);
	//set buttons
	//first button
	// for y=3 first button to y < states[state].length, to end(all buttons)
	for(y=3;y<states[state].length;y++){
		var button = document.createElement("IMG");
		for(x=0;x<states[state][y].length;x++){
			button.setAttribute(states[state][y][x][0], states[state][y][x][1]);
		}
		scene.appendChild(button);
	}

}

function title(){
	var gametitle = document.getElementsByClassName("gametitle");
	gametitle = gametitle[0];
	if(z != states[state][1].length){
		gametitle.innerHTML += states[state][1][z];
		z = z+1;
	}
	else{
		clearInterval(this);
	}
}
